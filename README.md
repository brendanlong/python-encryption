# Python Encryption Example

By [Brendan Long](mailto:self@brendanlong.com) ([website](https://www.brendanlong.com))

## About

This program was written to demonstrate how to correctly encrypt and decrypt
files, using [PBKDF2][pbkdf2]-[SHA256][sha256], [AES][aes]-128-[CFB][cfb], and
[HMAC][hmac]-[MD5][md5].

[aes]: https://en.wikipedia.org/wiki/Advanced_Encryption_Standard
[cfb]: https://en.wikipedia.org/wiki/Block_cipher_mode_of_operation#Cipher_feedback_.28CFB.29
[hmac]: https://en.wikipedia.org/wiki/Hash-based_message_authentication_code
[md5]: https://en.wikipedia.org/wiki/MD5
[pbkdf2]: https://en.wikipedia.org/wiki/PBKDF2
[sha256]: https://en.wikipedia.org/wiki/SHA-2

The output file is [JSON][json], containing all of the information that isn't
meant to be secret (the ciphertext, HMAC, IV, salt, and number of PBKDF2
iterations).

[json]: https://en.wikipedia.org/wiki/JSON

## Don't Use This Program

This program was written as a demonstration with two primary goals:

  * Correct and safe usage of crypto primitives.
  * Easily readable and understandable code.

As a result, efficiency was *not* a goal. In particular, the JSON output would make it very difficult to safely decrypt files without reading them entirely into memory. It also increases the file size by approximately 33% due to the need for base64-encoding.

So, if you're looking to understand how to correctly use crypto, then this program is for you, but if all you want is a command-line program to encrypt files for you, you should use [GPG][gpg] (or maybe [encfs][encfs] or [luks][luks])).

GPG has the advantage of (optionally) using public key encryption, which is *much* better than passwords, since they're many orders of magnitude harder to guess. If you do use GPG, I recommend using 4096-bit RSA keys and AES-256 (add `--cipher-algo aes256` to the command or [change your gpg.conf file][change-gpg-conf]).

[change-gpg-conf]: http://www.nas.nasa.gov/hecc/support/kb/Using-GPG-to-Encrypt-Your-Data_242.html
[encfs]: http://www.arg0.net/#!encfs/c1awt
[gpg]: https://www.gnupg.org/
[luks]: http://en.wikipedia.org/wiki/Linux_Unified_Key_Setup

## High-Level View

To encrypt, this program:

 1. Gets the password from the user (either as a command-line argument or with
    a prompt).

 2. Generates a 256-bit key using PBKDF2-SHA256, with the password, a random
    128-bit salt, and the configured number of iterations (defaults to 100,000).

 3. Splits the 256-bit key into two keys, `k1` (first 128 bits) and `k2` (last
    128 bits).

 3. Encrypts the input file using AES-CFB with `k1` and a random 128-bit IV.

 4. Creates an HMAC of the encrypted data using HMAC-MD5 and `k2`.

 5. Writes the number of iterations, salt, IV, encrypted data and HMAC to a
    JSON file (base64 encoded where necessary).

To decrypt, this program:

 1. Gets the password from the user (either as a command-line argument or with
    a prompt).

 2. Gets the information from the file.

 3. Regenerates `k1` and `k2` with the same method above.

 4. Recreates the HMAC of the encrypted data from the file, using `k2`.

 5. Checks the generated HMAC against the one from the file.

      * If this step fails, an error message is printed and we make no attempt
        to decrypt the data.

 5. Decrypts the input file using AES-CFB with `k2` and the IV from the file.

## More Detail

### PBKDF2

PBKDF2 is a [key derivation function](https://en.wikipedia.org/wiki/Key_derivation_function).
It takes a password and generates a key for use with an encryption function
(like AES). It is one of two good ways to securely hash a password, the other
one being [bcrypt](https://en.wikipedia.org/wiki/Bcrypt), which I recommend in
cases where you need to store and verify a password, like for login information.

When new programmers get security advice, they tend to get advice about how to
use a salt correctly, and which hash functions to use. All of this advice is
wrong. You should use PBKDF2 or bcrypt, and they will do these things correctly
without your input.

I chose PBKDF2 over bcrypt for this program because PBKDF2 generates an
arbitrary-length key (in this case, we want a key of the correct length for use
with AES). It's possible to do this with bcrypt, but it's not what it's designed
for, and the first rule of doing cryptography correctly is to use cryptographic
functions in the way they were meant to be used. bcrypt is for storing
passwords, PBKDF2 is for generating keys.

PyCrypto uses PBKDF2-SHA1 by default, meaning that internally, it uses SHA-1 as
part of the process. I initially left this at the default value because it
doesn't matter for security, but now it uses PBKDF2-SHA256 because we need two
128-bit keys (one for encryption, one for the MAC), and PBKDF2 is slow if the
output key size is larger than the hash function's output (and SHA-1's output
is only 128 bits).

### AES

The Advanced Encryption Standard (AES) is a secure and easy to use encryption
function. It was standardized in 2001, and has been heavily researched. In
cryptography, older is better, because old functions have been tested better
than new ones. CFB mode is used because it's used in
[PyCrypto's AES example][crypto-aes].

[pycryto-aes]: https://www.dlitz.net/software/pycrypto/api/current/Crypto.Cipher.AES-module.html

### HMAC

Encryption provides [confidentiality](https://en.wikipedia.org/wiki/Information_security#Confidentiality)
(someone with access to the encrypted file should not be able to read it), but
it doesn't provide [integrity](https://en.wikipedia.org/wiki/Information_security#Integrity)
(someone could change the data and you wouldn't be able to notice, except that
the decrypted data might look like garbage).

A hash-based authentication code (HMAC) allows you to verify that the encrypted
data wasn't changed in transit. HMAC uses a password and a hash function to
generate a hash. This program includes the HMAC in the JSON output. When it
decrypts a file, it generates the HMAC again and verifies that it hasn't
changed.

Sometimes, new programmers are taught about authentication, but they're usually
taught to do something like `sha1(data + password)`. This is wrong. Use HMAC.
This will protect you from things like flaws in your hash function (this program
uses HMAC-MD5, and it's perfectly safe, even though MD5 is broken in certain
cases).

## Usage

You can run `./encryption.py -h` for standard usage information and help.

### Encryption

To encrypt a file, use:

    :::bash
    ./encryption.py encrypt [-i input_file] [-o output_file] [-p password] [-n iterations]

All of the argument after `encrypt` are optional. If `-i` is not provided, input
will be read from the [standard input](https://en.wikipedia.org/wiki/Standard_streams#Standard_input_.28stdin.29)
(read from the terminal). If `-o` is not provided, output will be written to the
[standard output](https://en.wikipedia.org/wiki/Standard_streams#Standard_output_.28stdout.29)
(printed to the screen). If `-p` is not provided, you will prompted for the
password. In general, providing `-p` is insecure, because the password can be
saved to your shell's history. If `-n` is provided, it sets the number of
PBKDF2 iterations. By default this is 100,000. The higher the value is, the more
secure your password will be against brute-force attacks, but it will also take
longer to generate the key. This should be set at high as you're willing to
wait. For reference, iOS devices set this value to 10,000, so you probably
shouldn't set it any lower than that.

Errors and help messages will be printed to [standard error](https://en.wikipedia.org/wiki/Standard_streams#Standard_error_.28stderr.29),
so you can safely pipe the output (if you don't use `-o`):

    :::bash
    ./encryption.py encrypt -i input_file.txt > encrypted.json

### Decryption

To decrypt a file, use:

    :::bash
    ./encryption.py decrypt [-i input_file] [-o output_file] [-p password]

All of the argument after `decrypt` are optional. If `-i` is not provided, input
will be read from the [standard input](https://en.wikipedia.org/wiki/Standard_streams#Standard_input_.28stdin.29)
(read from the terminal). If `-o` is not provided, output will be written to the
[standard output](https://en.wikipedia.org/wiki/Standard_streams#Standard_output_.28stdout.29)
(printed to the screen). If `-p` is not provided, you will prompted for the
password. In general, providing `-p` is insecure, because the password can be
saved to your shell's history.

### Advanced Usage

This program accepts input on stdin and writes to stdout to allow you to use
[UNIX pipelines](https://en.wikipedia.org/wiki/Pipeline_%28Unix%29). If you add
a `|` between two commands in a UNIX terminal, the output from the first command
will be "piped" to the input of the second command.

#### Compression

Using that, we can do things like compressing the file before encrypting it.
This example uses [`xz`](https://en.wikipedia.org/wiki/Xz), which uses LZMA2
compression. You could do similar things with `gzip` or `bzip2`.

To compress with `xz` and encrypt:

    :::bash
    # Prompt for the password and store it as $pass
    # This prevents it from being saved in the shell's history
    read -sp 'Password: ' pass
    xz < example.txt | ./encryption.py encrypt -p $pass > secret.json
    # Clear $pass so other programs can't read it
    unset pass

To decrypt and decompress:

    :::bash
    read -sp 'Password: ' pass
    ./encryption.py decrypt -p $pass < secret.json | xz -d > output.txt
    unset pass

#### Encrypting a Hard Drive Backup

We can also use [`dd`](https://en.wikipedia.org/wiki/Dd_%28Unix%29) to make a
backup of a hard drive, compress it, and encrypt it:

    :::bash
    read -p 'Drive to back up (ex: /dev/sda1): ' drive
    read -sp 'Password: ' pass
    sudo dd if=$drive bs=4M | xz | ./encryption.py encrypt -p $pass -o backup.json
    unset pass

Restoring the backup. **This is an example. I don't actually recommend running
this on anything you care about**:

    :::bash
    read -p 'Drive to restore (ex: /dev/sda1): ' drive
    read -sp 'Password: ' pass
    ./encryption.py decrypt -p $pass -i backup.json | xz -d | sudo dd of=$drive bs=4M
    unset pass

Note that the output of this program isn't particularly space efficient, since
it uses JSON, so it's necessary to [base64 encode](https://en.wikipedia.org/wiki/Base64)
the encrypted output. In reality, you would probably want to use a more
efficient program for hard drive backups.
